import { browser, element, by, ElementFinder, ElementArrayFinder, ProtractorExpectedConditions, protractor } from 'protractor';

export class AmpReport {
	defaultTimeout: number = 120000;
  	EC: ProtractorExpectedConditions = protractor.ExpectedConditions;

	amp: ElementArrayFinder = element.all(by.css('.sfS3Pd'));
	ampDownload:ElementArrayFinder = element.all(by.css('.U26fgb.JRtysb.WzwrXb'));
	downloadCsv: ElementFinder = element(by.css('[aria-label="Download CSV"] .uyYuVb.oJeWuf'));
	email: ElementFinder = element(by.id('identifierId'));
	next: ElementFinder = element(by.id('identifierNext'));
	password: ElementFinder = element(by.css('#password input'));
	startNewButton: ElementFinder = element.all(by.css('.KXgdRe.qOvw5c .CwaK9 .RveJvd.snByac')).get(0);

	navigateTo() {
		return browser.get('https://www.google.com/webmasters/tools');
	}
}
