import { AmpReport } from './webmaster-report.po';
import { browser, protractor } from 'protractor';

describe('Webmaster Tools', () => {
	let page: AmpReport;

	beforeAll(() => {
		page = new AmpReport();
	});

	it('should download AMP errors report', () => {
		page.navigateTo();
		browser.wait(page.EC.elementToBeClickable(page.startNewButton), page.defaultTimeout);
		page.startNewButton.click();
		
		browser.wait(page.EC.elementToBeClickable(page.email), page.defaultTimeout);
		page.email.sendKeys('webtools@forbes.com');
		page.next.click();

		browser.wait(page.EC.elementToBeClickable(page.password), page.defaultTimeout);
		page.password.sendKeys('Forbes499');
		browser.actions().sendKeys(protractor.Key.ENTER).perform();
		
		browser.wait(page.EC.elementToBeClickable(page.amp.get(1)), page.defaultTimeout);
		page.amp.get(1).click();

		browser.wait(page.EC.elementToBeClickable(page.ampDownload.get(1)), page.defaultTimeout);
		page.ampDownload.get(1).click();

		browser.wait(page.EC.visibilityOf(page.downloadCsv), page.defaultTimeout);
		browser.get('https://search.google.com/search-console/export/amp/summary?resource_id=https%3A%2F%2Fwww.forbes.com%2F&hl=en&request_type=2&at=AC6dfzsLAAID9QEWbbHGp0-phrOQ:1542882916458');
	});
});
