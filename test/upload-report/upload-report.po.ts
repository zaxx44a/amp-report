import { browser, protractor, ProtractorExpectedConditions, ElementFinder, ElementArrayFinder, element, by } from 'protractor';

export class Box {
  defaultTimeout: number = 120000;
  EC: ProtractorExpectedConditions = protractor.ExpectedConditions;
  emailAddress: string = 'webtools@forbes.com';
  passwordText: string = 'Forbes499';

  email: ElementFinder = element(by.css('input[name="login"]'));
  filesList: ElementArrayFinder = element.all(by.css('.file-list-body.list-view > li'));
  notificationInfo: ElementFinder = element(by.css('.notification.info'));
  password: ElementFinder = element(by.css('input[name="password"]'))
  submitButton: ElementFinder = element(by.css('button[type="submit"]'));
  uploadFile: ElementArrayFinder = element.all(by.css('input[type="file"]'));
  uploadButton: ElementFinder = element(by.css('.btn.btn-primary.upload-menu-btn'));
  
  navigateTo() {
		return browser.get('https://forbes.account.box.com/login');
	} 
}
