import { browser, element, by } from 'protractor';
import { Box } from "./upload-report.po";
import * as path from 'path';
import * as glob from 'glob';

describe('Upload Report', () => {
  let page: Box;

  beforeAll(() => {
    page = new Box();
  });

  it('should upload AMP report', () => {
    page.navigateTo();

    browser.wait(page.EC.visibilityOf(page.email), page.defaultTimeout);
    page.email.sendKeys(page.emailAddress);
    browser.wait(page.EC.elementToBeClickable(page.submitButton), page.defaultTimeout);
    page.submitButton.click();

    browser.wait(page.EC.visibilityOf(page.password), page.defaultTimeout);
    page.password.sendKeys(page.passwordText);
    browser.wait(page.EC.elementToBeClickable(page.submitButton), page.defaultTimeout);
    page.submitButton.click();

    page.filesList.count().then((filesNumber: number) => {
        browser.driver.wait(() => {
            var filePattern = '*.csv';
            var filesArray = glob.sync('report/*', filePattern);
            if (typeof filesArray !== 'undefined' && filesArray.length > 0) {
                return filesArray;
            }
        }, page.defaultTimeout).then((filesArray) => {
            var filename = filesArray[0];
            const absolutePath = path.resolve(process.cwd(), filename);
            page.uploadFile.first().sendKeys(absolutePath);
            page.uploadButton.click();
        });

        browser.wait(page.EC.visibilityOf(page.notificationInfo), page.defaultTimeout);
        expect(page.notificationInfo.isDisplayed()).toBeTruthy();

        browser.wait(page.EC.stalenessOf(page.notificationInfo), page.defaultTimeout);
        page.filesList.count().then((filesNumberAfterUpload: number) => {
            expect(filesNumberAfterUpload).toBeGreaterThan(filesNumber);
        });
    });
  });
});
