// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const deleteReports = require('./removeReportFolder');

exports.config = {
	useAllAngular2AppRoots: true,
	allScriptsTimeout: 45000,
	specs: [
		'test/webmaster-report/webmaster-report.spec.ts',
		'test/upload-report/upload-report.spec.ts'
	],
	capabilities: {
		browserName: 'chrome',
		'chromeOptions': {
			prefs: {
                download: {
                    'prompt_for_download': false,
                    'default_directory': process.cwd() + '/report',
                }
			},
			args: ['--disable-dev-shm-usage', '--no-sandbox', '--window-size=11920, 1080'],
		}
	},
	directConnect: true,
	framework: 'jasmine',
	getPageTimeout: 10000,
	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 300000,
		print: function() {}
	},
	
	onComplete() {
    deleteReports.rmDir('./report');
  },
  
	onPrepare() {
		require('ts-node').register({
			project: 'test/tsconfig.e2e.json'
		});
		jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
		browser.waitForAngularEnabled(false);
	}
};
