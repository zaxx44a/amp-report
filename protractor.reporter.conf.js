// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const fs = require('fs');
const request = require('request');
const Reporter = require('test-runner-reporter').JasmineReporter;

/*global jasmine */
const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
    allScriptsTimeout: 45000,
	specs: [
		'test/webmaster-report/webmaster-report.spec.ts',
		'test/upload-report/upload-report.spec.ts'
	],
	capabilities: {
		browserName:'chrome',
		'chromeOptions': {
			prefs: {
                download: {
            		'prompt_for_download': false,
                	'default_directory': './report',
                },
			},
			args: ['--disable-dev-shm-usage', '--no-sandbox', '--disable-gpu'],
		}
	},
	directConnect: true,
	framework: 'jasmine',
	getPageTimeout: 10000,
	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 300000,
		print: function() {}
	},
    onPrepare() {
        require('ts-node').register({ project: './test/tsconfig.e2e.json' });
        browser.manage().window().setSize(1366, 768);
        jasmine.getEnv().addReporter(new Reporter());
        jasmine.getEnv().addReporter(new SpecReporter({
            spec: {
                displayStacktrace: false,
                displayDuration: true,
            },
            summary: {
                displayStacktrace: true,
                displayDuration: true
            },
            stacktrace: {
                filter: (stacktrace) => {
                    const lines = stacktrace.split('\n');
                    const filtered = [];
                    for (let i = 1; i < lines.length; i++) {
                        if (/e2e/.test(lines[i])) {
                            filtered.push(lines[i]);
                        }
                    }
                }
            }
        }));
        browser.waitForAngularEnabled(false);
    },
    afterLaunch: () => {
        return new Promise((fulfill, reject) => {
            const dirname = './report';
            fs.readdir(dirname, (err, files) => {
                let reports = [];
                files.forEach(file => {
                const report = JSON.parse(fs.readFileSync(`${dirname}/${file}`, { encoding: 'utf8' }));
                reports.push(report);
            });
            
            const endPoint = process.env.REPORT_ENDPOINT;
                var options = {
                    uri: endPoint,
                    method: 'POST',
                    json: {
                        report: reports
                    }
                };
            request(options, (err, res, body) => {
                if (err) {
                    reject(err);
                }
                fulfill();
            });
            });
        });
    }
};