let browserstack = require('browserstack-local');

exports.config = {
    seleniumAddress: 'http://hub-cloud.browserstack.com/wd/hub',
    allScriptsTimeout: 45000,
	specs: [
		'test/webmaster-report/webmaster-report.spec.ts',
		'test/upload-report/upload-report.spec.ts'
	],
    capabilities: {
        'browserstack.user': 'forbesmobile1',
        'browserstack.key': 'RZrutsXGJQ1Xg5ssfP5h',
        'browserstack.local': 'true',
        'build': 'AmpReport',
        'name': 'ARtest',
        'browserstack.debug': 'true',
        'browserstack.idleTimeout': '300',
        'os': 'OS X',
        'os_version': 'High Sierra',
        'browserName': 'Chrome',
        exclude: [],
    },
    framework: 'jasmine',
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 60000,
        print: function () { }
    },
    beforeLaunch: function () {
        return new Promise(function (resolve, reject) {
            killBrowserStackProcess().then(function () {
                let browserStackLocalFound = false;
                    if (exports.config.capabilities['browserstack.local'] === 'true' && !browserStackLocalFound) {
                        browserStackLocalFound = true;
                        exports.bs_local = new browserstack.Local();
                        exports.bs_local.start({ 'key': exports.config.capabilities['browserstack.key'] }, function (error) {
                            if (error) {
                                return reject(error);
                            }
                            resolve();
                        });
                    }
                
                if (!browserStackLocalFound) {
                    resolve();
                }
            });
        });
    },

    onPrepare() {
		require('ts-node').register({
			project: 'test/tsconfig.e2e.json'
		});
	},

    afterLaunch: () => {
        return new Promise(function (resolve, reject) {
            exports.bs_local.stop(resolve);
        });
    },
};

const killBrowserStackProcess = () => {
    return new Promise(function (resolve, reject) {
        const ps = require('ps-node');
        ps.lookup({ command: 'BrowserStackLocal' },
            function (err, resultList) {
                if (err) {
                    console.log(err);
                    throw new Error(err);
                }
                if (resultList.length == 0) {
                    resolve();
                }
                resultList.forEach(function (process) {
                    if (process) {
                        ps.kill(process.pid, function (err) {
                            if (err) {
                                reject();
                                throw new Error(err);
                            } else {
                                resolve();
                            }
                        });
                    }
                });
            }
        );
    });
}